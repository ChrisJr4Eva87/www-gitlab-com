---
layout: handbook-page-toc
title: "SOX Internal Controls"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Desktop Procedures

[Guidance on updating the Desktop Procedures](/handbook/finance/sox-internal-controls/guidence-on-updating-the-desktop-procedures/)

### 1.[Quote to Cash](/handbook/finance/sox-internal-controls/quote-to-cash/) 
### 2.[Procure to Pay](/handbook/finance/sox-internal-controls/procure-to-pay/)
### 3.[Hire to Retire](/handbook/finance/sox-internal-controls/hire-to-retire/)
### 4.[Regulatory](/handbook/finance/sox-internal-controls/regulatory/)
### 5.[Record to Report](/handbook/finance/sox-internal-controls/record-to-report/)
### 6.[Stock Compensation](/handbook/finance/sox-internal-controls/stock-compensation/)

<br>

## [Entity Level Controls](/handbook/finance/sox-internal-controls/entity-level-controls/)


## [IT General Controls](/handbook/finance/sox-internal-controls/it-general-controls/)


## [SOX Certifications](/handbook/finance/sox-internal-controls/sox-certifications/)

